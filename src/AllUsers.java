/**
 * this is AllUser class to implement blundr
 * @author mpranjal
 *
 */
public class AllUsers {

	/**
	 * User name,user age,User profile,User hobby
	 */
	public String name;
	public String age;
	public String profile;
	public String hobby;
	
	
	public void createNewBlunder(String name, String age, String profile, String hobby ) {
		// TODO Auto-generated method stub
		
		 this.name=name;
	     this.age=age;
	     this.profile=profile;
	     this.hobby=hobby;
	     System.out.println(this);
		
	}
	public String toString() {
   	 String stringToReturn=" ";
   	 stringToReturn+="name:      "+this.name +"\n";
   	 stringToReturn+="age:  "+this.age +"\n";
   	 stringToReturn+="profile:     "+this.profile +"\n";
   	 stringToReturn+="hobby:    "+this.hobby +"\n";

   	 return stringToReturn;
	
}
}
